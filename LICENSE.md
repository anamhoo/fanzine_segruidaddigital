Esta obra tiene una licencia CC BY-NC-SA 4.0

Redacción

Anamhoo

Edición y revisión

Codeando México

Diseño editorial e ilustraciones

Citlalli Dunne

- Anamhoo. 2020. Derechos Humanos y Seguridad Digital. Ed. Codeando México. México.  
