# Fanzine Derechos Humanos y Seguridad Digital

Este fanzine fue motivado para trabajar Seguridad Digital para defensores y defensoras de Derechos Humanos en América Latina. 

Ha sido creado recuperando experiencias pero también es producto de un aprendizaje colectivo por lo que debe ser considerada una obra hecha con mucho afecto desde lugares del mundo muy diferentes, múltiples diálogos y múltiples preocupaciones.

El fanzine se divide en 6 capítulos. En el primero se abordan aspectos generales sobre análisis de riesgos. El segundo da un panorama sobre la vigilancia a activistas en América Latina. El tercer capítulo nos da pistas para desarrollar estrategias. El capítulo cuatro introduce elementos importantes para pensar en las herramientas digitales que usamos. El capítulo cinco nos introduce en los ecosistemas tecnológicos. Finalmente el capítulo seis nos guía en algunas prácticas.

El texto se ha escrito pensando en que tanto facilitadores de talleres en seguridad digital como personas defensoras puedan utilizarlo.
Esperamos que lo disfrutes.

