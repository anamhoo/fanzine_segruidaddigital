## CAPÍTULO 03

El término “Seguridad Digital” no tiene una definición única y estática. Esto es porque la seguridad no es un estado absoluto, depende de varios factores, como el contexto de las personas y lo que perciben como “seguro”, de tal forma que la definición requiere una reflexión propia y colectiva. En este fanzine, nos referiremos más en sentido práctico al conjunto de herramientas y estrategias que emplearemos para protegernos al usar medios digitales, por ejemplo, teléfonos móviles o computadoras portátiles.

Cuando hablamos de lo digital hay cuatro aspectos que podemos tratar de proteger[Instituto de Genero y Tecnologías para defensoras de la tierra en América Latina. Alexandra Hache. https://is.gd/dpqzxV][9]:

- Ubicación
Dependiendo de la actividad que realizas y el tipo de dispositivo, la ubicación puede ser revelada por cosas como la dirección IP[IP viene del inglés Internet Protocol y hace referencia al protocolo que se usa para la transferencia de información entre un origen y un destino. Contempla intercambio de datos en redes y sistemas interconectados. Es un número que identifica, de manera lógica y jerárquica, a una Interfaz en red (elemento de comunicación/conexión) de un dispositivo (computadora, tableta, portátil, smartphone) y es asignada por el proveedor de servicios de telecomunicaciones. Dirección IP. Wikipedia. https://is.gd/vrMuC7
][10] que tienes asignada.

- Contenido
De nuestras comunicaciones.

- Información
Personal Sensible e Información Personal Identificable[La información personal sensible son los datos de una persona que si se revelan puede tener un efecto negativo, como su cuenta bancaria o sus contraseñas de correo. La Información personal identificable (PII por sus siglas en inglés) se refiere a toda aquella información sobre un individuo que es administrada por un tercero (p. ej. gobiernos o empresas) incluyendo todos aquellos datos que pueden ser distintivos del individuo o a partir de los cuales es posible rastrear su identidad y toda la información asociada o asociable al individuo (por ejemplo el número de su DNI, su dirección física, la placa de matriculación de su coche, etc). Pueden leer más en Mortazavi, M. y Salah, K. 2015. Privacy and Big Data in Privacy in a Digital Networked World. Información Personal Sensible. NA.][11]

- Redes sociales y contactos 
Aquí nos referimos no a tus cuentas en redes sociales comerciales, sino a quienes efectivamente constituyen tus redes.

El dilema es que proteger implica también emplear más energía (tiempo-recursos), por lo que es necesario priorizar.

Las estrategias de resistencia[Strategies Of Resistance. My Shadow. https://is.gd/VnzwgX][12] podemos dividirlas también en cuatro:

* Reducir
En algunas circunstancias menos es más. Por ejemplo, a veces tenemos muchos correos electrónicos (el primero que tuve y le puse un nombre chistoso, el personal, el del trabajo, etc.) y muchas veces nos reenviamos correos entre ellos para respaldar. Eso va dejando rastros de nuestra información, ¡por muchos lados!

* Ofuscar
Ofuscar es tratar de confundir a quien busque atacarnos. Podemos crear muchos perfiles sobre uno mismo con información diversa que confunda sobre cuál es el perfil verdadero.

* Compartimentar
Compartimentar es dar un lugar a cada cosa. Por ejemplo, usar un teléfono para las actividades del trabajo y otro para las personales.

* Fortificar
Fortificar es tratar de poner barreras más poderosas, como contraseñas fuertes en todas nuestras cuentas.

Lo que hay que recordar es que no hay recetas sobre cómo tener mayor seguridad digital, depende de cada caso y es un proceso que se construye de acuerdo a las necesidades y prioridades de cada persona.
