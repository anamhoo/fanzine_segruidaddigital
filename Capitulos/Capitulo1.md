# CAPÍTULO 01

Ante el contexto en el que se desarrollan las actividades de las personas defensoras de derechos humanos en América Latina, su seguridad es una preocupación constante. En la práctica, existen muchos elementos que dificultan la creación y puesta en marcha de planes integrales de protección, como la falta de tiempo, recursos y/o conocimiento.

Considerando la seguridad desde una perspectiva holística, lo digital se integra como una dimensión más a la seguridad física y la seguridad psico-social. Comúnmente, al menos de forma intuitiva, al generar planes de protección se prioriza la seguridad física, pese a que en diferentes reportes y encuestas se ha mostrado cómo las personas defensoras de derechos humanos tienen incidentes de seguridad digital
y se sienten vulnerables al usar tecnología.

Esto se debe a que actualmente la tecnología funciona como una caja negra en la que sólo vemos lo que ocurre al final del camino, pero desconocemos cómo se llevan a cabo los procesos y como están hechos los dispositivos que usamos, lo cual dificulta entender en dónde están las vulnerabilidades y cómo protegerse.

## Actividad 
¿Alguna vez has hecho un análisis de riesgo? 

Existen diferentes metodologías para desarrollarlos. Ahora te proponemos una actividad muy sencilla pero poderosa para que reflexiones sobre la importancia de la seguridad digital en tu contexto [Esta actividad es resultado de reflexiones de mujeres latinoamericanas y sigue en construcción, puedes mejorarla y compartirla a quien la necesite.][1].

En la siguiente imagen:

1. Escribe la acción sobre la cual harás tu análisis. Ej:, “Comunicación por grupos de WhatsApp” (puede ser un grupo en específico)

2. Piensa en amenazas que pudieran afectar esta acción. Ej: “Robo de celular”, “Captura de pantalla por parte de algún integrante de los grupos”, “Que el teléfono quede sin capacidad de almacenar información”.

3. Para los Riesgos, Vulnerabilidades e Impactos ilumina el color que corresponde de acuerdo a tu contexto.

## Definiciones

* Amenaza: algo que me hace daño (considera que en algunos casos aún cuando la amenaza no se haya concretado su posibilidad de existencia puede dañar).

* Riesgo (R): la posibilidad de que se materialice el daño (rojo: posibilidad alta, naranja: media, verde: baja).

* Vulnerabilidad/capacidad (V): ¿qué tan preparada estoy para enfrentar esta amenaza? (rojo: poco preparada, naranja: medio preparada, verde: muy preparada).

* Impacto (I): si pasa, ¿qué tan grave sería? (rojo: gravísimo, naranja: más o menos grave, verde: no es grave).

Toma un momento para observar tu actividad completa, ¿qué tan roja, amarilla o verde se ve?, ¿qué te hace sentir?, ¿hay alguna acción que puedas tomar para prevenir o contrarrestar estas amenazas?
