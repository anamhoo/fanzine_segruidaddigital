## CAPÍTULO 02

En 2016, una de las organizaciones que ha dado seguimiento a casos de vigilancia y espionaje digital en contra de defensores de derechos humanos, CitizenLab publicó el reporte “El disidente del millón de dólares”, el cual documentó el uso de tres tipos de productos informáticos para interceptar comunicaciones del activista Ahmed Mansoor, ganador del premio Martin Ennals Award (conocido también como el “Premio Nobel para los derechos humanos”).

Mansoor, residente de los Emiratos Árabes Unidos, fue atacado con programas de las compañías Finfisher, Hacking Team y NSO Group [The Million Dollar Dissident. Bill Marczak and John Scott-Railton. https://is.gd/odqSkg][1]. De acuerdo con los reporte de estas compañías sus productos son vendidos de forma legal para gobiernos democráticos, fuerzas de la ley y agencias de inteligencia para combatir amenazas como crimen organizado o terroristas. Sin embargo, lo ocurrido con Mansoor revela su uso contra defensores de derechos humanos.

Tal vez esta historia suene geográficamente muy lejana, no obstante, existen evidencias de que la censura, la vigilancia y el espionaje en medios digitales han crecido también en la región latinoamericana. Por ejemplo, a partir de la exposición de información reservada de Hacking Team se reveló que mantuvieron negociaciones en al menos 13 países en América Latina [Hacking Team en América Latina. Derechos Digitales. https://is.gd/8rJ0kE][2] y México fue el país que más regalías dio a esta compañía al gastar 5,808,875 euros[Hacking Team Emails Expose Proposed Death Squad Deal, Secret U.K. Sales Push And Much More. The Intercept. https://is.gd/bKpN8W][3].

Hacking Team vendió a diferentes entidades gubernamentales su plataforma, conocida como Control Remoto Galileo, para interceptar información. En el caso de Puebla, durante el periodo del gobernador
Rafael Moreno Valle, Galileo fue usado contra periodistas, adversarios políticos y académicos[El gobierno de Puebla usó el software de Hacking Team para espionaje político. Animal Político. https://is.gd/utZoOB][4].

Desafortunadamente y pese a la exposición pública de estos casos, se han seguido adquiriendo programas para la vigilancia para ser usados contra personas defensoras de derechos humanos.

En la última actualización[Reckless VII. The Citizen Lab. https://is.gd/Xu4PrE][5] (marzo, 2019) para México de la investigación del uso de Pegasus, el sistema vendido por NSO Group, se confirman 25 blancos entre los que se encuentran periodistas, abogados de derechos humanos, legisladores y el Grupo Interdisciplinario de Expertos Independientes (GIEI) que participó en la investigación del caso Ayotzinapa.

¿Cómo opera Pegasus?

Hasta ahora lo que se ha visto es que el ataque inicia cuando recibes un mensaje de texto (sms) acompañado por un link. La información que contiene el mensaje está encaminada a que tu primer impulso sea abrir el link sin reflexionarlo. Por ejemplo, en el Caso de Griselda Triana, esposa de Javier Valdéz, recibió estos dos mensajes:

 “Proceso: PGR asegura que el móvil del asesinato de Javier Valdéz fue para robarse su automóvil. Detalles: LINK*”

 “¿Cómo ves este reportaje? Cuando faltan las palabras sólo ataques podemos recibir: LINK*”

El LINK aparenta ser de un sitio web legítimo, sin embargo, estás entrando a un sitio que buscará instalar un programa informático malicioso (malware) que iniciará un ataque para tomar el control del dispositivo. El sistema está diseñado para no dejar rastros desde dónde se hicieron estos ataques. Si se logra la infección, el atacante logrará tener acceso total al dispositivo. En el caso de teléfonos móviles podrá acceder al micrófono, cámara, mensajes de texto, escuchar llamadas telefónicas, contenido de aplicaciones, etc.

**El link original se puede ver en la fuente 5**

En México, el costo que tuvo la contratación de Pegasus es desconocido y existen evidencias de irregularidades en su adquisición[Pegasus, posible negocio redondo que involucra a altos funcionarios: MCCI. Aristegui Noticias. https://is.gd/rXN3iv][6]. Para darnos una idea, en 2016 el New York Times reveló que para la infección de 10 iPhones el costo es de $650,000 dólares más una membresía de $500,000 dólares[How Spy Tech Firms Let Governments See Everything on a Smartphone. The New York Times. https://is.gd/x3yp96][7].

Para conocer más puedes ver la investigación de “Gobierno espía”[Gobierno Espía. ARTICLE 19 et al. https://is.gd/MERKvz][8].
