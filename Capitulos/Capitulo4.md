## CAPÍTULO 04

Es común que las herramientas digitales que se van integrando a nuestro trabajo no han sido elecciones informadas. Tal vez inicialmente se usaron porque son de las compañías más grandes en el mercado y las que más se promocionan, porque ofrecen servicios supuestamente “gratuitos” o porque son las que venían instaladas en los dispositivos. Esto hace que las herramientas no sólo puedan ser inseguras, sino que además no sean las más eficientes.

Pero ¿cómo se puede hacer una elección informada de herramientas digitales?

Primero considera todos los usos, necesidades y recursos con los que cuentas. Después, investiga qué herramientas existen que sean útiles para lo que tu requieres y que se encuentren dentro de lo que definiste en el primer paso. Por último, investiga el compromiso con la privacidad y la seguridad de la herramienta y si efectivamente lo aplican.

Aquí dos guías que te ayudarán:

Guía 01. Tu herramienta[Modificado de Technology tools in human rights. 2016. Engine Room https://is.gd/VESCq0][13]


1. ¿Qué haces? y ¿Qué te gustaría hacer?

¿Colectar? ¿Analizar? ¿Publicar? ¿Almacenar?

2. ¿Qué condiciones tendrás al usar la herramienta?

¿Se requiere acceso a internet o telefonía móvil?

3. ¿En qué dispositivos?¿Qué sistemas operativos soporta?

¿Se puede instalar en teléfonos móviles y computadoras? ¿Se puede instalar en sistemas Windows, Mac o Gnu-Linux?

4. Si será usado por un equipo de trabajo, ¿se cuenta con los recursos necesarios?

¿Idiomas necesarios? ¿Configuraciones técnicas? ¿Conocimientos especializados? ¿Existen manuales? ¿Pago de licencia de uso?



### Guía 02. Evalúa la herramienta 

¿Cómo puede una persona no “especialista” evaluar una herramienta desde la seguridad y la privacidad? La mejor respuesta que hemos encontrado es: haciendo más preguntas e investigando.

Por ejemplo, iniciemos con el dinero. ¿Cómo se sostiene esta herramienta? Podría sostenerse por donaciones, ofreciendo servicios o ¡vendiendo tus datos!

También podrías querer conocer qué tan transparente es la compañía/organización que mantiene la herramienta: ¿hace informes de transparencia?, cuando se han reportado fallos de seguridad, ¿da respuestas públicas?, ¿cómo son sus políticas de privacidad? ¿son claras?, cuando recibe requerimientos gubernamentales, ¿los reporta?

Existen también algunos aspectos técnicos como si la herramienta fue auditada, si usa mecanismos de cifrado[En criptogafía el cifrado es el procedimiento para convertir una serie de datos o información en una cifra o código de manera que solo pueda interpretarse y ser leída por aquella persona que tenga la manera de revertir el proceso, por ejemplo, con una contraseña o una clave de cifrado. Cifrado. NA][14] o si su código es libre (software libre)[Software que respeta la libertad de los usuarios y la comunidad. A grandes rasgos, significa que los usuarios tienen la libertad de ejecutar, copiar, distribuir, estudiar, modificar y mejorar el software. Software libre. NA.][15].

Si quieres conocer más revisa los materiales de Género y Tecnología[16. Recursos de Género y Tecnología. Tactical Tech. https://is.gd/0osExy][16] y la sección de documentos de Técnicas Rudas[Recursos de Técnicas Rudas. Técnicas Rudas. https://is.gd/QsK0Zj][17].
