## CAPÍTULO 06 

Hasta aquí tal vez todo suena muy bien, tecnologías sociales, ecosistemas tecnológicos, estrategias y herramientas para la seguridad digital, pero al final del día ¿cómo lo integramos en nuestras vidas?

Es posible que ya tengas prácticas integradas y/o que ya uses algunas de las herramientas y lo que falta puede parecer mucho, pero hay que recordarnos que todo es un proceso y puede llevar un tiempo.

Ahora veremos ejemplos simples para ir dando los primeros pasos:

### Mapas

¿Cómo recopilamos y visualizamos información geográfica, tanto para fines de análisis interno como para la generación de mapas públicos? Por facilidad, tendemos a usar Google Maps. Google es uno de los hipergigantes de la tecnología, con ganancias cuantiosas comparables con el Producto Interno Bruto de algunos países, prácticas laborales nocivas[Fuck off, Google! https://is.gd/jDTsR6][18], profundo impacto ambiental y explotación de usuarios/as[¿Territorio internet? Espacios, afectividades y comunidades. la_jes. https://is.gd/cWEwBj][19][How to stop data centres from gobbling up the world’s electricity. Nature. https://is.gd/nop16b][20].

De forma alternativa existe Open Street Map (openstreetmap.com), un proyecto colaborativo de información geográfica que está pensado para ser alimentado y usado por cualquier persona. Es posible usarlo
dentro de dos aplicaciones que te podrían ser útiles: OsmAnd y Umap.

OsmAnd es una aplicación de mapas y navegación para teléfonos móviles que integra datos de OpenStreetMap y de Wikipedia para que puedan ser usados incluso sin conexión a internet. Con la integración de extensiones puedes trazar tus propios puntos o rutas, tomar imágenes o vídeos georeferenciados y también alimentar la base de datos de OpenStreetMap.

Con esta herramienta puedes recopilar y administrar información geográfica, así que puedes mantenerla privada o ponerla pública. Si decides ponerla pública, puedes hacerlo en OpenStreetMap directo desde la aplicación o en Umap[Por ejemplo https://umap.openstreetmap.co/es/ uMap. uMap][21], una fantástica herramienta para hacer mapas que estarán disponibles por internet.

### Documentos

Muchas veces tenemos que compartir documentos que no podemos enviar por correo electrónico, así que es posible que recurramos a servicios comerciales (incluso si no te cobran por usarlos su modelo de negocio está basado en el lucro).

Una alternativa es usar share.riseup.net. Es bastante intuitivo:

1. Entrarás a un sitio web con un cuadro que dice “upload” (subir), dale click.

2. Se abrirá un explorador de archivos, selecciona el archivo que quieres cargar.

3. Mientras se guarda, el contenido se cifra y al finalizar el link del sitio ha cambiado. Copia el link y compártelo con quienes tú elijas.

### Comunicaciones

Es posible que tus comunicaciones instantáneas las hagas preferentemente por WhatssApp. Esta aplicación pertenece a una compañía nefasta que está al servicio del poder, Facebook, la cual ha tenido múltiples reportes de fallos de seguridad (el más reciente fue una vulnerabilidad que permitía a un atacante hacer una llamada que no sería registrada por el usuario y que lograría inyectar código malicioso)[WhatsApp voice calls used to inject Israeli spyware on phones. Financial Times. https://is.gd/MZhtCP][22].

Una alternativa más segura e incluso divertida es Wire. Wire tiene un modelo de negocio basado en ofertar servicios, tiene estándares de cifrado fuertes y su política de privacidad integra que sus servidores operen bajo las legislaciones que dan la mayor protección a sus usuarios/as.

Algunas funcionalidades son el poder crear grupos, hacer llamadas/video-llamadas grupales, compartir ubicación, distorsionar la voz en mensajes, desaparecer automáticamente mensajes, entre otras. Está
disponible para teléfonos y computadoras de escritorio y para diferentes sistemas operativos.

Estos son sólo tres ejemplos de herramientas útiles pero antes de usarlas hay dos preguntas fundamentales que te puedes hacer:

* ¿Cómo integrarías estas herramientas dentro de tu flujo de trabajo?

* ¿Cómo podrían formar parte de tu estrategia de seguridad?
