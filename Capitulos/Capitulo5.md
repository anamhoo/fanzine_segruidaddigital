## CAPÍTULO 05

Una forma eficiente para combinar la seguridad digital junto con las dinámicas de trabajo es pensar en términos de ecosistema tecnológico.

Lo interesante de abordarlo desde la perspectiva de los ecosistemas es que permite visualizar comunidades, sus interacciones internas y con otras comunidades, infraestructura, plataformas y programas
específicos. Nos permite también integrar dimensiones menos tangibles como la dimensión política sobre la justicia social (libertad vs vigilancia y censura, obsolescencia programada, explotación de territorios y de personas vs trabajo digno y la búsqueda de la sustentabilidad en el uso de recursos), etc.

El proceso de usar herramientas tecnológicas (o peor aún que las herramientas tecnológicas nos usen) al estar dentro de las dimensiones de los ecosistemas tecnológicos nos ayuda a movernos hacia las tecnologías sociales, a lograr soberanía/autonomía tecnológica para los pueblos.

La soberanía tecnológica nos remite a la contribución que hacemos cada una de nosotras al desarrollo de tecnologías, rescatando nuestros imaginarios radicales, rescatando nuestra historia y memorias colectivas, re-situándonos para poder soñar y desear juntas la construcción aquí y ahora de nuestras infraestructuras propias de información, comunicación y expresión.
- Alexandra Haché, Soberanía Tecnológica

*Ecosistema de Seguridad Digital*{Esquema}
